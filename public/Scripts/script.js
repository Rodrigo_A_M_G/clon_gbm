function showDropdown(dropdownId) {
    document.getElementById(dropdownId).classList.remove('hidden');
}

function hideAllDropdowns() {
    document.querySelectorAll('.dropdown').forEach(function (dropdown) {
        dropdown.classList.add('hidden');
    });
}

document.getElementById('invierte-link').addEventListener('mouseenter', function () {
    hideAllDropdowns();
    showDropdown('invierte-dropdown');
});

document.getElementById('nosotros-link').addEventListener('mouseenter', function () {
    hideAllDropdowns();
    showDropdown('nosotros-dropdown');
});

document.getElementById('login-link').addEventListener('mouseenter', function () {
    hideAllDropdowns();
    showDropdown('login-dropdown');
});

document.querySelector('header').addEventListener('mouseleave', function () {
    hideAllDropdowns();
});
// ACTIVIDAD 6
// ACTIVIDAD 6
const carousel = document.getElementById('carousel');
const prevBtn = document.getElementById('prevBtn');
const nextBtn = document.getElementById('nextBtn');
let currentIndex = 0;

prevBtn.addEventListener('click', () => {
    currentIndex = Math.max(currentIndex - 1, 0);
    updateCarousel();
});

nextBtn.addEventListener('click', () => {
    const maxIndex = carousel.children.length - 1;
    currentIndex = Math.min(currentIndex + 1, maxIndex);
    updateCarousel();
});

function updateCarousel() {
    const cardWidth = 500;
    const offset = -currentIndex * (cardWidth);
    carousel.style.transform = `translateX(${offset}px)`;
}
// END ACTIVIDAD 6
// END ACTIVIDAD 6
// Función para animar el contador Actividad 10
function animateValue(id, start, end, duration) {
    var obj = document.getElementById(id);
    var range = end - start;
    var current = start;
    var increment = end > start ? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var timer = setInterval(function() {
        current += increment;
        obj.innerHTML = '+' + current;
        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}
animateValue("counter1", 0, 37, 2000); 
setTimeout(function() {
    animateValue("counter2", 0, 2, 1500);
}, 1000);
setTimeout(function() {
    animateValue("counter3", 0, 500, 1000);
}, 2000);
